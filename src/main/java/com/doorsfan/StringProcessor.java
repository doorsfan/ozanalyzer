package com.doorsfan;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

class StringProcessor {

    private String outputFormat;
    private String inputDelimiter;
    private String outputDelimiter;
    private String filter;
    private LinkedHashMap<String, Version> versions;
    private ArrayList<String> fields;

    private static final Pattern TOKENS =
            Pattern.compile("(\\s+)|(AND)|(OR)|(>=)|(<=)|(=\\w|=\\s)|(>)|(<)|(\\()|(\\))|(\\$\\w+)|((([1-9]|([012][0-9])|(3[01]))[-/]([0]{0,1}[1-9]|1[012])[-/]\\d\\d\\d\\d [012]{0,1}[0-9]:[0-6][0-9])|[^\\).]+)");

    private enum TokenType {
        WHITESPACE, AND, OR, MORE_EQUALS, LESS_EQUALS, EQUALS, MORE, LESS, LEFT_PAREN, RIGHT_PAREN, IDENTIFIER, LITERAL, EOF
    }

    StringProcessor(String outputFormat, String inputDelimiter, String outputDelimiter, String fields, String filter, LinkedHashMap<String, Version> versions) {
        this.outputFormat = outputFormat;
        this.inputDelimiter = inputDelimiter;
        this.outputDelimiter = outputDelimiter;
        this.filter = filter;
        this.versions = versions;
        if (!(isNull(fields) || fields.isEmpty())) {
            this.fields = Arrays.stream(fields.split(",")).map(String::trim).collect(Collectors.toCollection(ArrayList::new));
        }
    }

    private void filterLine(String[] tokens) {

        String versionName = tokens[0].trim() + tokens[1].trim();
        Version currentVersion = versions.get(versionName);
        StringBuilder outputLine = new StringBuilder();
        String iterField;
        String iterValue;
        String iterDelimeter;
        Iterator<String> fieldsIter;
        IExpr expr = null;
        Map<String, String> tokensMap = tokensToMap(tokens);
        try {
            expr = parse(tokenize(filter));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (!expr.evaluate(tokensMap)) {
            return;
        }

        if (fields != null) {
            fieldsIter = fields.iterator();
        } else {
            fieldsIter = currentVersion.getFields().iterator();
        }

        int index;
        while (fieldsIter.hasNext()) {
            iterField = fieldsIter.next();
            index = currentVersion.indexOfField(iterField);
            iterValue = index > -1 ? tokens[index] : "";
            if (fieldsIter.hasNext()) {
                iterDelimeter = outputDelimiter;
            } else {
                iterDelimeter = "";
            }
            if (outputFormat.equalsIgnoreCase("fields")) {
                outputLine.append(iterValue).append(iterDelimeter);
            } else {
                outputLine.append(String.format("%s = %s%s", iterField, iterValue, iterDelimeter));
            }
        }
        System.out.println(outputLine.toString());
    }

    private Map<String, String> tokensToMap(String[] tokens) {
        String versionName = tokens[0].trim() + tokens[1].trim();
        Version currentVersion = versions.get(versionName);
        Iterator<String> fieldsIter = currentVersion.getFields().iterator();

        Map<String, String> tokensAsMap = new LinkedHashMap<>();
        String key;
        String value;
        int index = 0;
        while (fieldsIter.hasNext()) {
            key = fieldsIter.next().replaceAll("\\s+", "");
            value = index < tokens.length ? tokens[index++] : "";
            tokensAsMap.put(key, value);
        }
        return tokensAsMap;
    }

    void processLine(String line) {
        String[] tokens = Arrays.stream(line.split(inputDelimiter)).map(String::trim).toArray(String[]::new);
        if (tokens[2].toLowerCase().trim().equals("datetime")) {
            registerVersion(tokens);
        } else {
            filterLine(tokens);
        }
    }

    private void registerVersion(String[] tokens) {
        String[] trimmedTokens = Arrays.stream(tokens).map(String::trim).toArray(String[]::new);
        Version version = new Version(trimmedTokens);
        String tokenVersion = trimmedTokens[0] + trimmedTokens[1];
        versions.putIfAbsent(tokenVersion, version);
    }

    private static class Token {
        final TokenType type;
        final int start; // start position in input (for error reporting)
        final String data; // payload

        Token(TokenType type, int start, String data) {
            this.type = type;
            this.start = start;
            this.data = data;
        }

        @Override
        public String toString() {
            return type + "[" + data + "]";
        }
    }

    private static TokenStream tokenize(String input) throws ParseException {
        Matcher matcher = TOKENS.matcher(input);
        List<Token> tokens = new ArrayList<>();
        int offset = 0;
        TokenType[] types = TokenType.values();
        while (offset != input.length()) {
            if (!matcher.find() || matcher.start() != offset) {
                throw new ParseException("Unexpected token at " + offset, offset);
            }
            for (int i = 0; i < types.length; i++) {
                if (matcher.group(i + 1) != null) {
                    if (types[i] != TokenType.WHITESPACE)
                        tokens.add(new Token(types[i], offset, matcher.group(i + 1).replaceFirst("^\\$", "")));
                    break;
                }
            }
            offset = matcher.end();
        }
        tokens.add(new Token(TokenType.EOF, input.length(), ""));
        return new TokenStream(tokens);
    }

    private static class TokenStream {
        final List<Token> tokens;
        int offset = 0;

        TokenStream(List<Token> tokens) {
            this.tokens = tokens;
        }

        // consume next token of given type (throw exception if type differs)
        Token consume(TokenType type) throws ParseException {
            Token token = tokens.get(offset++);
            if (token.type != type) {
                throw new ParseException("Unexpected token at " + token.start
                        + ": " + token + " (was looking for " + type + ")",
                        token.start);
            }
            return token;
        }

        Token consume(List<TokenType> type) throws ParseException {
            Token token = tokens.get(offset++);
            if (!type.contains(token.type)) {
                throw new ParseException("Unexpected token at " + token.start
                        + ": " + token + " (was looking for " + type + ")",
                        token.start);
            }
            return token;
        }

        // consume token of given type (return null and don't advance if type differs)
        Token consumeIf(TokenType type) {
            Token token = tokens.get(offset);
            if (token.type == type) {
                offset++;
                return token;
            }
            return null;
        }

        @Override
        public String toString() {
            return tokens.toString();
        }
    }

    private static class CompareExpr implements IExpr {
        private final String identifier;
        private final String literal;
        private final Token comparator;

        CompareExpr(TokenStream stream) throws ParseException {
            Token token = stream.consumeIf(TokenType.IDENTIFIER);
            List comparators = new ArrayList<>(Arrays.asList(TokenType.EQUALS,
                    TokenType.LESS, TokenType.LESS_EQUALS, TokenType.MORE, TokenType.MORE_EQUALS));
            if (token != null) {
                this.identifier = token.data;
                this.comparator = stream.consume(comparators);
                this.literal = stream.consume(TokenType.LITERAL).data;
            } else {
                this.literal = stream.consume(TokenType.LITERAL).data;
                this.comparator = stream.consume(comparators);
                this.identifier = stream.consume(TokenType.IDENTIFIER).data;
            }
        }

        @Override
        public String toString() {
            return identifier + comparator.data + literal;
        }

        @Override
        public boolean evaluate(Map<String, String> data) {
            boolean res = false;
            String logValue = data.get(identifier);
            if (isNull(literal) || isNull(logValue)) {
                return res;
            }
            int numericalLiteral = 0;
            int numericalLogValue = 0;
            boolean isNumber = false;
            LocalDateTime dateTimeLiteral = strToLocalDateTime(literal);
            LocalDateTime dateTimeLogValue = strToLocalDateTime(logValue);
            if (isNumeric(literal) && isNumeric(logValue)) {
                numericalLiteral = Integer.parseInt(literal);
                numericalLogValue = Integer.parseInt(logValue);
                isNumber = true;
            }
            switch (comparator.type) {
                case MORE:
                    res = isNumber ?
                            numericalLogValue > numericalLiteral :
                            isNull(dateTimeLiteral) || isNull(dateTimeLogValue) ?
                                    logValue.compareTo(literal) > 0 :
                                    dateTimeLogValue.compareTo(dateTimeLiteral) > 0;
                    break;
                case LESS:
                    res = isNumber ?
                            numericalLogValue < numericalLiteral :
                            isNull(dateTimeLiteral) || isNull(dateTimeLogValue) ?
                                    logValue.compareTo(literal) < 0 :
                                    dateTimeLogValue.compareTo(dateTimeLiteral) < 0;
                    break;
                case EQUALS:
                    res = literal.equals(data.get(identifier));
                    break;
                case LESS_EQUALS:
                    res = isNumber ?
                            numericalLogValue <= numericalLiteral :
                            isNull(dateTimeLiteral) || isNull(dateTimeLogValue) ?
                                    logValue.compareTo(literal) <= 0 :
                                    dateTimeLogValue.compareTo(dateTimeLiteral) <= 0;
                    break;
                case MORE_EQUALS:
                    res = isNumber ?
                            numericalLogValue >= numericalLiteral :
                            isNull(dateTimeLiteral) || isNull(dateTimeLogValue) ?
                                    logValue.compareTo(literal) >= 0 :
                                    dateTimeLogValue.compareTo(dateTimeLiteral) >= 0;
                    break;
            }
            return res;
        }

        private static boolean isNumeric(String str) {
            return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
        }

        private static LocalDateTime strToLocalDateTime(String str) {
            LocalDateTime dateTime = null;
            if (str.matches("\\d{2}[-/]\\d{2}[-/]\\d{4}\\s\\d{2}:\\d{2}")) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("[dd[-][/]MM[-][/]yyyy HH:mm]");
                dateTime = LocalDateTime.parse(str, formatter);
            } else if (str.matches("\\d{2}[-/]\\d{2}[-/]\\d{4}")) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("[dd[-][/]MM[-][/]yyyy]");
                dateTime = LocalDate.parse(str, formatter).atStartOfDay();
            }
            return dateTime;
        }
    }


    private static class SubExpr implements IExpr {
        private final IExpr child;

        SubExpr(TokenStream stream) throws ParseException {
            if (stream.consumeIf(TokenType.LEFT_PAREN) != null) {
                child = new OrExpr(stream);
                stream.consume(TokenType.RIGHT_PAREN);
            } else {
                child = new CompareExpr(stream);
            }
        }

        @Override
        public String toString() {
            return "(" + child + ")";
        }

        @Override
        public boolean evaluate(Map<String, String> data) {
            return child.evaluate(data);
        }
    }

    private static class AndExpr implements IExpr {
        private final List<IExpr> children = new ArrayList<>();

        AndExpr(TokenStream stream) throws ParseException {
            do {
                children.add(new SubExpr(stream));
            } while (stream.consumeIf(TokenType.AND) != null);
        }

        @Override
        public String toString() {
            return children.stream().map(Object::toString).collect(Collectors.joining(" AND "));
        }

        @Override
        public boolean evaluate(Map<String, String> data) {
            for (IExpr child : children) {
                if (!child.evaluate(data))
                    return false;
            }
            return true;
        }
    }

    private static class OrExpr implements IExpr {
        private final List<IExpr> children = new ArrayList<>();

        OrExpr(TokenStream stream) throws ParseException {
            do {
                children.add(new AndExpr(stream));
            } while (stream.consumeIf(TokenType.OR) != null);
        }

        @Override
        public String toString() {
            return children.stream().map(Object::toString).collect(Collectors.joining(" OR "));
        }

        @Override
        public boolean evaluate(Map<String, String> data) {
            for (IExpr child : children) {
                if (child.evaluate(data))
                    return true;
            }
            return false;
        }
    }

    private static IExpr parse(TokenStream stream) throws ParseException {
        OrExpr expr = new OrExpr(stream);
        stream.consume(TokenType.EOF); // ensure that we parsed the whole input
        return expr;
    }
}
