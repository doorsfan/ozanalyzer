package com.doorsfan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


class DbConnector {

//    private static final String DEFAULT_MYSQL = "jdbc:mysql://localhost:3306/";

    static Connection getConnection(String url, String db, String user, String password) throws SQLException {
        System.setProperty("db.driver", "com.mysql.cj.jdbc.Driver");
        String formatUrl = url + db + "?useLegacyDatetimeCode=false&amp&serverTimezone=UTC";
        return DriverManager.getConnection(formatUrl, user, password);
    }
}
