package com.doorsfan;

import java.util.Map;

public interface IExpr {

        boolean evaluate(Map<String, String> data);
    }
