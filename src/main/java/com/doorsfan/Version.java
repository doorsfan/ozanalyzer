package com.doorsfan;

import java.util.ArrayList;
import java.util.Arrays;

class Version {
    private String versionName;

    private ArrayList<String> fields;

    Version(String[] tokens) {
        this.versionName = tokens[0] + tokens[1];
        this.fields = new ArrayList<>(Arrays.asList(tokens));
    }

    ArrayList<String> getFields() {
        return fields;
    }

    int indexOfField(String fieldName) {
        return fields.indexOf(fieldName);
    }

}
