package com.doorsfan;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.doorsfan.DbConnector.getConnection;
import static java.util.Objects.requireNonNull;

class DbReader {

    private final String jdbc;
    private final String db;
    private final String user;
    private final String password;

    DbReader(String jdbc, String db, String user, String password) {
        requireNonNull(jdbc);
        requireNonNull(db);
        requireNonNull(user);
        requireNonNull(password);
        this.jdbc = jdbc;
        this.db = db;
        this.user = user;
        this.password = password;

    }

    //todo use int from, int quantity: " limit " + quantity + " offset " + from
    List<String> read(String table, String column) {

        requireNonNull(table);
        requireNonNull(column);
        List<String> records = new ArrayList<>();
        readRecords(table, column, records);
        return records;
    }

    private void readRecords(String table, String column, List<String> records) {
        try (Connection connection = getConnection(jdbc, db, user, password)) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(String.format("SELECT %s FROM %s;", column, table))) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        records.add(resultSet.getString(2));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
