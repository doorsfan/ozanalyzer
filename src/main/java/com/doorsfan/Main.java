package com.doorsfan;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.File;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.Callable;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Command(description = "Prints the checksum (MD5 by default) of a file to STDOUT.",
        name = "checksum", mixinStandardHelpOptions = true, version = "checksum 3.0")
public class Main implements Callable<Integer> {

    @Option(names = {"--jdbc"}, description = "Database connection string i.e. jdbc:mysql://localhost:3306/")
    private String jdbc;

    @Option(names = {"--db"}, description = "Database name")
    private String db;

    @Option(names = {"--table"}, description = "Table name")
    private String table;

    @Option(names = {"--column"}, description = "Table name")
    private String column;

    @Option(names = "--user", description = "Database user name")
    private String user = "";

    @Option(names = "--password", description = "Database password")
    private String password;

    @Option(names = {"--file"}, description = "Name of file to parse")
    private File filename;

    @Option(names = "--output-format", description = "field=value (by default) or fields")
    private String outputFormat = "field=value";

    @Option(names = "--input-delimiter", description = "Delimiter in csv file/DB record, by default ','")
    private String inputDelimiter = ",";

    @Option(names = "--output-delimiter", description = "Delimiter in csv file/DB record, by default ','")
    private String outputDelimiter = ",";

    @Option(names = "--fields", description = "Fields to output, comma-separated; if absent - output all")
    private String fields;

    @Option(names = "--filter", description = "Filter expression, f.e. \"($createdAt>=14/08/2019 13:00 and $ErrorLevel>50) or $deviceID=yyy\"")
    private String filter = "";

    public static void main(String... args) {
        int exitCode = new CommandLine(new Main()).execute(args);
        System.exit(exitCode);
    }

    @Override
    public Integer call() {
        LinkedHashMap<String, Version> versions = new LinkedHashMap<>();
        StringProcessor stringProcessor = new StringProcessor(outputFormat,
                inputDelimiter,
                outputDelimiter,
                fields,
                filter,
                versions);
        List<String> records;
        if (isNull(jdbc) && isNull(filename)) {
            System.out.println("No source declared! Try to use option -h");
            return 2;
        }
        if (nonNull(jdbc) && !jdbc.isEmpty()) {
            DbReader dbReader = new DbReader(jdbc, db, user, password);
            records = dbReader.read(table, column);
        } else {
            CsvReader  csvReader = new CsvReader();
            records = csvReader.read(Paths.get(filename.getAbsolutePath()));
        }
        records.forEach(stringProcessor::processLine);
        return 0;
    }
}
