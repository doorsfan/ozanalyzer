package com.doorsfan;

import org.junit.Test;

import java.io.File;
import java.nio.file.Paths;

public class CsvReaderTest {
    private CsvReader reader = new CsvReader();

    @Test
    public void read() {
        reader.read(Paths.get("./src/test/fixtures/files/test.csv")).forEach(System.out::println);
    }

    @Test
    public void read1() {
        reader.read(new File("./src/test/fixtures/files/test.csv")).forEach(System.out::println);
    }
}
