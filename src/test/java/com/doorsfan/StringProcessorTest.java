package com.doorsfan;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;

public class StringProcessorTest {
    List<String> records;
    StringProcessor stringProcessor;
    StringProcessor stringProcessor2;
    @Before
    public void setUp() throws Exception {
        LinkedHashMap<String, Version> versions = new LinkedHashMap<>();
        String outputFormat = "field=value";
        String inputDelimiter = ",";
        String outputDelimiter = ",";
        String fields = "";
//        String filter = "$UserId = tal";
//        String filter = "($DeviceId = xxxx AND $ErrorLevel <900) OR $UserId = tal";
//        String filter = "$ErrorLevel >= 50";
        String filter = "($Datetime >= 14/08/2019 11:40 AND $ErrorLevel > 50) OR $Action = New User";
//        String filter = "$DeviceId = xxxx OR $IP=10.0.0.1";
        stringProcessor = new StringProcessor(outputFormat,
                inputDelimiter,
                outputDelimiter,
                fields,
                filter,
                versions);
        String customFields = "Action, IP, Datetime";

        stringProcessor2 = new StringProcessor(outputFormat,
                inputDelimiter,
                outputDelimiter,
                customFields,
                filter,
                versions);
        CsvReader reader = new CsvReader();
        records=reader.read(Paths.get("./src/test/fixtures/files/test.csv"));

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void processLine() throws Exception {
        setUp();
        records.forEach(stringProcessor::processLine);
        records.forEach(stringProcessor2::processLine);

    }
}
