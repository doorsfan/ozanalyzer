Analyzer Task
Input data
There are two types of log records (M-Master and S-Slave) in the following format
Example of LogRecord both in CSV file and Database
First time you have combination of (M/S and Version) there is line describing the fields
Example of a file:

M, 1, Datetime, Error Level, DeviceId, UserId, Message
M, 1, 14/08/2019 11:39, 4, xxxx, tal, “Initialization of the system, and loading”
M, 1, 14/08/2019 11:40, 100, xxxx, tal, “Open Connection”
S, 1, Datetime, Error Level, DeviceId, Action, Message
M, 1, 14/08/2019 11:40, 100, xxxx, tal, “Close Connection, and reboot”
S, 1, 14/08/2019 11:40, 6, xxxx, New User, We created the user in the systems
S, 1, 14/08/2019 11:41, 3, xxxx, User logged in, User tal logged in
M, 1, 14/08/2019 11:39, 4, xxxx, tal, “Initialization of the system”
S, 1, 14/08/2019 11:40, 6, New User, We created the user in the systems, Yurii added it
S, 1, 14/08/2019 11:41, 3, User logged in, “User tal logged in, and accessed the system”
M, 2, Datetime, Error Level, DeviceId, UserId, MobileId, Message
S, 1, 14/08/2019 11:42, 3, User logged in, User tal logged in, ve odpaam added by Yurii
M, 2, 14/08/2019 11:43, 100, yyy, yura, 12345, Message
M, 2, 14/08/2019, 100, yyy, yura, 1234, Message
S, 2, Datetime, Error Level, DeviceId, IP, Action, Message
S, 2, 14/08/2019, 200, yyy, 10.0.0.1, Allocate IP from DHCP, DHCP worked

Database Table
In the table there are only two columns:
Datetime-record creation datetime createdAt
LogRecord-string

Parameters to Command Line (CLI) using argv and argc (part of the command line)
command line with the following parameters:
Input 
Connection string to MySQL database with log records
Path for CSV log file (should work w/o database !!!)
Column filter - which fields to display example: Datetime, DeviceId, IP, MobileId (if there is no value to field i.e. IP return empty field)
Row filter - expression (similar to awk) example: ($createdAt>=14/08/2019 13:00 and $ErrorLevel>50) or $deviceID=yyy
Format (i.e. field=value or fields with Field Separator)
Field Separator for example: space, tab, comma ...

Output
Stdout (i.e. printf)
